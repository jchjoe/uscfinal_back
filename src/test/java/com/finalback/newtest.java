package com.finalback;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bean.Product;
import com.controller.ProductController;
import com.dao.ProductDao;
import com.service.ProductService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@WebMvcTest(value = ProductService.class, excludeAutoConfiguration = { SecurityAutoConfiguration.class })
@RunWith(MockitoJUnitRunner.class)
public class newtest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @MockBean
    private ProductDao productDao;

    @MockBean
    private ProductController productController;

    @Mock
    List<Product> products;
    @Before
    public void before() {
        //获取mockmvc对象实例
        mockMvc = MockMvcBuilders.webAppContextSetup().build();
    }

    @Test
    public void testGetAllProducts_success() throws Exception {

        Product p1 = new Product(1, "name1", "5", "des1","pic1","show");
        Product p2 = new Product(2, "name2", "15", "des2","pic2","show");
        Product p3 = new Product(3, "name3", "25", "des3","pic3","show");
        products = new ArrayList<>();
        products.add(p1);
        products.add(p2);
        products.add(p3);
        when(productService.test()).thenReturn(products);


    }



}
